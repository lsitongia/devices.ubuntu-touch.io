module.exports = {
  extends: [
    'plugin:vue/recommended',
    'plugin:md/recommended',
    'plugin:prettier/recommended',
    'prettier'
  ],
  rules: {
    'prettier/prettier': 'error',
    'vue/no-v-html': "off",
    "md/remark": "off"
  },
  overrides: [
    {
      files: ['*.md'],
      parser: 'markdown-eslint-parser',
      rules: {
        'prettier/prettier': [
          'error',
          { parser: 'markdown' },
        ],
      },
    },
  ]
}
